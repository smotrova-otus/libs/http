package http

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-kit/kit/auth/jwt"
	kitendpoint "github.com/go-kit/kit/endpoint"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/smotrova-otus/libs/log"
	"gitlab.com/smotrova-otus/libs/uuid-go"
	"io"
	"net/http"
	"strconv"

	kithttp "github.com/go-kit/kit/transport/http"
	"gitlab.com/smotrova-otus/libs/errors"
)

type Error struct {
	Err      error
	HTTPCode int
}

func (e Error) Error() string {
	return e.Err.Error()
}

func NewError(err error, HTTPCode int) *Error {
	return &Error{Err: err, HTTPCode: HTTPCode}
}

func (e Error) StatusCode() int {
	return e.HTTPCode
}

func (e Error) MarshalJSON() ([]byte, error) {
	s := struct {
		Error string `json:"error"`
	}{
		Error: e.Err.Error(),
	}

	return json.Marshal(s)
}

// ErrorEncoder is responsible for encoding an error to the ResponseWriter.
// For errors without json.Marshaller returns Internal error.
func ErrorEncoder(ctx context.Context, err error, w http.ResponseWriter) {
	log.Must(ctx).WithError(err).Info("error encoding")

	if err == jwt.ErrTokenContextMissing ||
		err == jwt.ErrTokenInvalid ||
		err == jwt.ErrTokenExpired ||
		err == jwt.ErrTokenMalformed ||
		err == jwt.ErrTokenNotActive ||
		err == jwt.ErrUnexpectedSigningMethod {
		err = errors.ErrUnauthorized
	}

	// go-kit.DefaultErrorEncoder doesn't respond with json
	// for errors which is not implement json.Marshaler,
	// for those type of errors return InternalError.
	// go-kit/kit@v0.8.0/transport/http/server.go:180
	if _, ok := err.(json.Marshaler); !ok {
		err = errors.ErrInternal
	}

	kithttp.DefaultErrorEncoder(ctx, err, w)
}

// RawResponder is checked by EncodeJSONResponse. It should return io.Reader to copy it to http.ResponseWriter
type RawResponder interface {
	RawResponse() io.Reader
}

// EncodeJSONResponse is a EncodeResponseFunc that serializes the response as a
// JSON object to the ResponseWriter. Its works like standard go-lit EncodeJSONResponse but
// checks Failer and RawResponder interfaces
func EncodeJSONResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if failer, ok := response.(kitendpoint.Failer); ok && failer.Failed() != nil {
		ErrorEncoder(ctx, failer.Failed(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if headerer, ok := response.(kithttp.Headerer); ok {
		for k, values := range headerer.Headers() {
			for _, v := range values {
				w.Header().Add(k, v)
			}
		}
	}

	code := http.StatusOK
	if sc, ok := response.(kithttp.StatusCoder); ok {
		code = sc.StatusCode()
	}
	w.WriteHeader(code)
	if code == http.StatusNoContent {
		return nil
	}

	if rawResponse, ok := response.(RawResponder); ok {
		_, err := io.Copy(w, rawResponse.RawResponse())
		return err
	}

	return json.NewEncoder(w).Encode(response)
}

func NewLoggerRequestMiddleware(logger *logrus.Entry) kithttp.RequestFunc {
	return func(ctx context.Context, _ *http.Request) context.Context {
		logger = logger.WithField("requestId", ctx.Value(kithttp.ContextKeyRequestXRequestID))
		return log.ContextWithLogger(ctx, logger)
	}
}

func DecodeIntQueryParam(req *http.Request, name string) (int, bool, error) {
	valueStr := req.URL.Query().Get(name)
	if valueStr == "" {
		return 0, false, nil
	}
	valueInt, err := strconv.ParseInt(valueStr, 10, 32)
	if err != nil {
		return 0, true, err
	}
	return int(valueInt), true, nil
}

func DecodeUUIDQueryParam(req *http.Request, name string) (uuid.UUID, bool, error) {
	valueStr := req.URL.Query().Get(name)
	if valueStr == "" {
		return uuid.Nil, false, nil
	}
	value, err := uuid.Parse(valueStr)
	if err != nil {
		return uuid.Nil, true, err
	}
	return value, true, nil
}

func DecodeUUIDRouteParam(name string, req *http.Request) (uuid.UUID, error) {
	vars := mux.Vars(req)
	if vars == nil {
		return uuid.Nil, fmt.Errorf("there are no mux vars")
	}

	raw, ok := vars[name]
	if !ok {
		return uuid.Nil, fmt.Errorf("there are no route param: %s", name)
	}

	return uuid.Parse(raw)
}
