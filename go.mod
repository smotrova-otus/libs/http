module gitlab.com/smotrova-otus/libs/http

go 1.13

require (
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.7.0
	gitlab.com/smotrova-otus/libs/errors v0.0.0-20200930212602-ddf8d9e05a0c
	gitlab.com/smotrova-otus/libs/log v0.0.0-20200930212234-71890bdc7b33
	gitlab.com/smotrova-otus/libs/uuid-go v0.0.0-20200930210328-802b0a203c52
)
